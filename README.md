これは
---------------
このリポジトリではアニメ関係の情報を管理します。  
bitbucketで管理するというのもアレですが…  

## 管理するデータ
* 新規アニメの視聴、または録画リスト  
* 視聴時間・録画時間などの情報  
* L字やテロップ、周期変更などの情報  
* その他(サボらなければ今後増えていくでしょう…)

## リンク関連
[Twitter](https://twitter.com/amanelia)  
[しょぼいカレンダー](http://cal.syoboi.jp/)  

## Changelog
2015.04.02 新規作成  
2015.07.18 2015-Q3追加  
2015.10.11 2015-Q4追加  
2016.01.16 2016-Q1追加  
2016.04.16 2016-Q2追加  
2016.07.03 2016-Q3追加  
2016.10.05 2016-Q4追加  
2017.01.22 2017-Q1追加  
2017.04.16 2017-Q2追加  
2017.07.12 2017-Q3追加  
2017.10.04 2017-Q4追加  
2018.01.15 2018-Q1追加
2018.05.10 2018-Q2追加
2018.08.17 2018-Q3追加
2018.10.23 2018-Q4追加
2019.02.01 2019-Q1追加
